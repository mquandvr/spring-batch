/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import org.springframework.batch.core.ItemReadListener;

/**
 * @author Minh Quan
 *
 */
public class ItemReaderListener implements ItemReadListener<Report> {

    @Override
    public void beforeRead() {
        System.out.println("Before reader");
    }

    @Override
    public void afterRead(Report item) {
        System.out.println("after reader");

    }

    @Override
    public void onReadError(Exception ex) {
        System.out.println("Read error");

    }


}
