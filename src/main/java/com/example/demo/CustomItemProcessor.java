/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;

/**
 * @author Minh Quan
 *
 */
@StepScope
public class CustomItemProcessor implements ItemProcessor<Report, Report> {

    public Report process(Report itemObj) throws Exception {
        System.out.println("Processing " + itemObj);
        itemObj.setStaffName(itemObj.getStaffName().substring(0, 1).toUpperCase() + itemObj.getStaffName().substring(1));
//        throw new Exception("test");
        return itemObj;
    }
}
