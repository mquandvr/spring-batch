/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @author Minh Quan
 *
 */
@StepScope
public class Step2FailTasklet implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        System.out.println("Step 2 FAIL-----------------");
        return RepeatStatus.FINISHED;
    }

}
