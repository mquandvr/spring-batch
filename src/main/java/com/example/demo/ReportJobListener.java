/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * @author Minh Quan
 *
 */
public class ReportJobListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {

        System.out.println("Before Job");

    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("After Job");
    }

}
