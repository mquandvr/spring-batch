/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * @author Minh Quan
 *
 */
public class ReportStepListener implements StepExecutionListener {

    @Override
    public void beforeStep(StepExecution stepExecution) {
        System.out.println("Before step----" + stepExecution);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println("after step----" + stepExecution);
//        if (ExitStatus.FAILED.equals(stepExecution.getExitStatus())) {
//        }
//        System.out.println(stepExecution.getExitStatus().getExitCode());
//        stepExecution.setExitStatus(ExitStatus.FAILED);
        return stepExecution.getExitStatus();
    }

}
