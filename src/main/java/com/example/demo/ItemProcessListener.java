/*
 * Copyright mquandvr 2021
 */
package com.example.demo;


/**
 * @author Minh Quan
 *
 */
public class ItemProcessListener implements org.springframework.batch.core.ItemProcessListener<Report, Report> {

    @Override
    public void beforeProcess(Report item) {
        System.out.println("Before process " + item);
    }

    @Override
    public void afterProcess(Report item, Report result) {
        System.out.println("After process " + item);
        System.out.println("After process result " + result);

    }

    @Override
    public void onProcessError(Report item, Exception e) {
        // TODO Auto-generated method stub
        System.out.println("Process error " + item);
    }



}
