/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Minh Quan
 *
 */
@StepScope
public class Step2Tasklet implements Tasklet {

    @Value("#{jobParameters[inputParam]}")
    private String parameter1;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        System.out.println("Step 2-----------------");
        System.out.println("Parameter-----------------" + parameter1);
        String status = deleteFolder(parameter1);
        System.out.println("DELETE FILE STATUS: " + status);

        if (status.equals("NOT EXISTS")
                || status.contains("ERROR")
                || status.equals("ERROR")) {
//            return RepeatStatus.CONTINUABLE;
            throw new IOException(status);
        }

        return RepeatStatus.FINISHED;
    }

    private String deleteFolder(String param) {
        String result = "";
        Path pathFile = Paths.get("E:\\test\\" + param);
        try {
            if (Files.notExists(pathFile)) {
                result = "NOT EXISTS";
            } else if (pathFile.toFile().listFiles().length != 0) {
                Files.walkFileTree(pathFile, new SimpleFileVisitor<Path>() {


                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        if (dir.equals(pathFile)) {
                            return FileVisitResult.TERMINATE;
                        }

                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }

                });
                result = "SUCCESS";
            } else {
                result = "EMPTY";
            }
        } catch (IOException e) {
            result = "ERROR--- " + e.getMessage();
        }

        return result;
    }

}
