/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Minh Quan
 *
 */
public class AppMain {

    static Job jobObj;
    static JobLauncher jobLauncherObj;
    static ApplicationContext contextObj;
    private static String[] springConfig  = {"spring/batch/config/spring-beans.xml" };

    public static void main(String[] args) {
        // Loading The Bean Definition From The Spring Configuration File
        contextObj = new ClassPathXmlApplicationContext(springConfig);

        jobObj = (Job) contextObj.getBean("helloWorldJob");
        jobLauncherObj = (JobLauncher) contextObj.getBean("jobLauncher");
        Map<String, JobParameter> parameter = new HashMap<String, JobParameter>();
        parameter.put("inputParam", new JobParameter("12345"));
        try {
            JobExecution execution = jobLauncherObj.run(jobObj, new JobParameters(parameter));
            System.out.println("Exit Status : " + execution.getStatus());
        } catch (Exception exceptionObj) {
            exceptionObj.printStackTrace();
        }
        System.out.println("Done");
    }
}
