/*
 * Copyright mquandvr 2021
 */
package com.example.demo;

import java.util.List;

import org.springframework.batch.core.ItemWriteListener;

/**
 * @author Minh Quan
 *
 */
public class ItemWriterListener implements ItemWriteListener<Report> {

    @Override
    public void beforeWrite(List<? extends Report> items) {
        System.out.println("Before write " + items.size());
    }

    @Override
    public void afterWrite(List<? extends Report> items) {
        System.out.println("After write " + items.size());
    }

    @Override
    public void onWriteError(Exception exception, List<? extends Report> items) {
        System.out.println("Write error " + items.size());
    }

}
